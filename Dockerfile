FROM registry.gitlab.com/pantacor/pvr:utils-develop

RUN apk update &&  \
	apk add --no-cache \
	jq \
	cryptsetup \
	make \
	git \
	curl \
	tar \
	xz \
	bzip2 \
	gzip \
	tar \
	zstd \
	bash \
	losetup \
	squashfs-tools \
	coreutils \
	fakeroot && \
	rm -rf /var/cache/apk/*

COPY ./personalize /usr/bin/personalize
COPY ./get_packages /usr/bin/get_packages
COPY ./get_image /usr/bin/get_image
COPY ./flash_image /usr/bin/flash_image
COPY ./container-ci/download_gitlab_package /usr/bin/download_gitlab_package

WORKDIR /usr/bin

ARG GITLAB_TOKEN
ENV PVR_DISABLE_SELF_UPGRADE=true
ENV DOWNLOAD_LATEST=true 

RUN PACKAGE_NAME=pvtx download_gitlab_package pantacor/pv-platforms/pvr-sdk "$GITLAB_TOKEN" | tar zxf -

WORKDIR /work
